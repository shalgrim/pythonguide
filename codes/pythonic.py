# pythonic.py

import math
import time

class Ring(object):
    """ Here we will see the actual logic behind various pieces of Python
    language e.g. instances, variables, method and @property etc. 
    Also, we will see the combine usage of these pieces to complete a 
    design with Agile methodology. 
    """
    
    # class variables
    date = time.strftime("%Y-%m-%d", time.gmtime()) # today's date "YYYY-mm-dd"
    center = 0.0 # center of the ring 

    def __init__(self, date=date, metal="Copper", radius=5.0, 
                price=5.0, quantity=5):
        """ init is not the constructor, but the initializer which
        initialize the instance variable 

        self : is the instance

        __init__ takes the instance 'self' and populates it with the radius, 
        metal, date etc. and store in a dictionary. 

        self.radius, self.metal etc. are the instance variable which
        must be unique. 
        """

        self.date = date
        self.metal = metal
        self.radius = radius
        self.price = price
        self.quantity = quantity

    @property
    # method-name should be same as attribute i.e. 'radius' here
    def radius(self):
        # return self._radius # _radius can be changed with other name
        return self.diameter/2 # _radius can be changed with other name

    @radius.setter
    def radius(self, val):
        # 'val' should be float or int
        if not isinstance(val, (float, int)):
            raise TypeError("Expected: float or int")
        # self._radius = float(val) 
        self.diameter = 2 * float(val) 

    # Multiple constructor
    # below constructor is added for the 'research organization' who are
    # doing their work based on diameters, 
    @classmethod
    def diameter_init(cls, diameter):
        radius = diameter/2;  # change diameter to radius
        return cls(radius) # return radius

    @staticmethod  # meter to centimeter conversion
    def meter_cm(meter):
        return(100*meter)

    def cost(self):
        return self.price * self.quantity

    def area(self):
        # return math.pi * self.radius**2
        # p = self.perimeter() # wrong way to calculate perimeter
        p = self.__perimeter()  # use local copy of perimeter()
        r = p / ( 2 * math.pi)
        return math.pi * r**2 

    def perimeter(self):
        return 2 * math.pi * self.radius
    
    # local copy can be created in the lines after the actual method
    __perimeter = perimeter # make a local copy of perimeter

def main():
    # print("Center of the Ring is at:", Ring.center) # modify class variable
    # r = Ring(price=8) # modify only price
    # print("Radius:{0}, Cost:{1}".format(r.radius, r.cost()))
    # print("Radius:{0}, Perimeter:{1:0.2f}".format(r.radius, r.perimeter()))

    print() # check the new constructor 'diameter_init'
    d = Ring.diameter_init(diameter=10)
    print("Radius:{0}, Perimeter:{1:0.2f}, Area:{2:0.2f}".format(
        d.radius, d.perimeter(), d.area()))
    # print("Radius:{0}, Perimeter:{1:0.2f}".format(d.radius, d.perimeter()))
    # m = 10 # 10 meter 
    # print("{0} meter = {1} centimeter".format(m, d.meter_cm(m)))

if __name__ == '__main__':
    main()
