# find_ex.py

import argparse
import sys

from pathlib import Path

parser = argparse.ArgumentParser(description="Find command")

# positional argument when defined without --
# nargs='*' or '?' is required for default-positional-arg values
# * : read all command line arguments;  ?: read one command line arg 
parser.add_argument('location', type=str, nargs="*", default='.')

# optional argument when defined with --
# no 'nargs' i.e. it's str (not list of str)
parser.add_argument('--name', type=str, default="*") 

# possible values are "d", "f" and "all"
parser.add_argument('--type',type=str,default="all",choices=["d", "f", "all"])

args = parser.parse_args() # save arguments in args
loc = Path(args.location[0])

items=[]
for l in loc.rglob(args.name):
    if args.type == "d" and l.is_dir():
        items.append(l)
    elif args.type == "f" and l.is_file():
        items.append(l)
    elif args.type == "all":
        items.append(l)

# print output
for i in items:
    print(i)
