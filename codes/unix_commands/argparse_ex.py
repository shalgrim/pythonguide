# argparse_ex.py

import argparse

def mulNum(a):
    p = 1
    for item in a:
        p *= item
    return p 

parser = argparse.ArgumentParser(description="First argparse code")

# read  list of integers
parser.add_argument(
        type=int, # type int
        nargs='+', # read multiple values in a list
        dest='integers', # name of the attribute returned by argparse
        help='read list of integer: 1 2 3')

parser.add_argument(
        '-p', '--product', # name -p or --product 
        dest='multiply', # by default 'dest=product'
        action='store_const', # store values
        const=mulNum, # call function 'mulNum'
        help='product of integers'
    )

parser.add_argument(
        '--sum', 
        dest='accumulate', 
        action='store_const',
        const=sum, # inbuilt method 'sum' to add the values of list 
        help='sum of integers' 
    )

args = parser.parse_args() # save arguments in args

# if --sum is in command 
if args.accumulate:
    sum = args.accumulate(args.integers)
    print("sum =", sum)

# if '-p or --product' is in command 
if args.multiply != None:
# if args.multiply is not None:
    # pass arg.integers to arg.multiply, which calls function 'mulNum'
    product = args.multiply(args.integers) 
    print("product = ", product)


####### Execution ####################


######## Help ############
# $ python argparse_ex.py -h  (or $ python argparse_ex.py --help)
# usage: argparse_ex.py [-h] [-p] [--sum] integers [integers ...]

# First argparse code

# positional arguments:
  # integers       read list of integer: 1 2 3

# optional arguments:
  # -h, --help     show this help message and exit
  # -p, --product  product of integers
  # --sum          sum of integers

############ Results ######### 
# $ python argparse_ex.py 2 5 1 --sum
# sum = 8

# $ python argparse_ex.py 2 5 1 -p
# product =  10

# $ python argparse_ex.py 2 5 1 --product
# product =  10

# $ python argparse_ex.py 2 5 1 -p --sum
# sum = 8
# product =  10

# $ python argparse_ex.py -p 2 5 1 --sum
# sum = 8
# product =  10
