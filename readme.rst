See below link, for complete list of tutorials,
===============================================

http://pythondsp.readthedocs.io/en/latest/pythondsp/toc.html


The tutorial is available at following link,
============================================

http://pythonguide.readthedocs.io/en/latest/

About
=====

Advance Python features are discussed e.g. decorator, descriptor and property etc.