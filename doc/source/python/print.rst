Print statement
***************

Introduction
============

In this chapter, the print statement is discussed to print the output in nice format. 

Expansion calculation
=====================

In the below code, number of days are calculate in which the diameter of the ring becomes greater than or equal to 10 cm. 

    .. code-block:: python
        :linenos:
        :emphasize-lines: 16-17, 23

        # expansion.py

        # find the number of days when radius = 10 cm due to heat-expansion

        import sys

        if len(sys.argv) != 3: # display error message for missing arguments
            raise SystemExit("usage : ring.py \"metal\" radius")

        # sys.argv[0] is the file name 
        metal = sys.argv[1] 

        # input is read as string therefore it is converted into float
        radius = float(sys.argv[2])

        # list of expansion rate for different metal
        rate = [0.4, 0.08, 0.05] # [Copper, Gold, Iron]

        day = 0
        while radius < 10:
            # multiply by correct expansion rate
            if metal == "Copper":
                expansion = radius * rate[0]
            elif metal == "Gold":
                expansion = radius * rate[1] 
            elif metal == "Iron":
                expansion = radius * rate[2]
            else: 
                print("Enter the correct metal")
                break

            # new radius
            radius += expansion
            day += 1 # increment the number of days by one

        # print the number of days
        print("Number of days =", day)


Following are the outputs for different metals with same radius, 

    .. code-block:: text
    
        $ python expansion.py "Gold" 8 
        Number of days = 3
        
        $ python expansion.py "Iron" 8
        Number of days = 5
        
        $ python expansion.py "Copper" 8
        Number of days = 1
        
        $ python expansion.py "Silver" 8
        Enter the correct metal
        Number of days = 0
          


Print the expansion
===================

In the below code, the new radius after expansion is printed on the daily basis, 

    .. code-block:: python
        :linenos:
        :emphasize-lines: 21, 38-39
    
        # expansion.py

        # find the number of days when radius = 10 cm due to heat-expansion

        import sys

        if len(sys.argv) != 3: # display error message for missing arguments
            raise SystemExit("usage : ring.py \"metal\" radius")

        # sys.argv[0] is the file name 
        metal = sys.argv[1] 

        # input is read as string therefore it is converted into float
        radius = float(sys.argv[2])

        # list of expansion rate for different metal
        rate = [0.4, 0.08, 0.05] # [Copper, Gold, Iron]

        day = 0

        print("day, expansion, radius")
        while radius < 10:
            # multiply by correct expansion rate
            if metal == "Copper":
                expansion = radius * rate[0]
            elif metal == "Gold":
                expansion = radius * rate[1] 
            elif metal == "Iron":
                expansion = radius * rate[2]
            else: 
                print("Enter the correct metal")
                break

            # new radius
            radius += expansion
            day += 1 # increment the number of days by one
            
            # print the data
            print(day, expansion, radius)

        # print the number of days
        # print("Number of days =", day)


Following is the output for Gold ring, 

    .. code-block:: text
    
        $ python expansion.py "Gold" 8 
        day, expansion, radius
        1 0.64 8.64
        2 0.6912 9.3312
        3 0.746496 10.077696000000001  


Formatted output
================

In the below code, the 'format' option of print statement is used to display the output in more readable form, 

    .. code-block:: python
        :linenos:
        :emphasize-lines: 21-23, 25-27, 29-30, 48-51
    
        # expansion.py

        # find the number of days when radius = 10 cm due to heat-expansion

        import sys

        if len(sys.argv) != 3: # display error message for missing arguments
            raise SystemExit("usage : ring.py \"metal\" radius")

        # sys.argv[0] is the file name 
        metal = sys.argv[1] 

        # input is read as string therefore it is converted into float
        radius = float(sys.argv[2])

        # list of expansion rate for different metal
        rate = [0.4, 0.08, 0.05] # [Copper, Gold, Iron]

        day = 0

        ## use any of the below
        ## below is not in good format
        # print("{0}, {1}, {2}".format("day", "expansion", "radius"))

        ## "> right aligned" "< left aligned"
        ## 5s : string with width 5
        print("{:>5s} {:>10s} {:>7s}".format("day", "expansion", "radius"))

        ## old style
        # print("%5s %10s %7s" % ("day", "expansion", "radius"))

        while radius < 10:
            # multiply by correct expansion rate
            if metal == "Copper":
                expansion = radius * rate[0]
            elif metal == "Gold":
                expansion = radius * rate[1] 
            elif metal == "Iron":
                expansion = radius * rate[2]
            else: 
                print("Enter the correct metal")
                break

            # new radius
            radius += expansion
            day += 1 # increment the number of days by one
            
            ## print the data
            ## 5d : 5 digits 
            ## 7.2f : 7 digits with 2 decimal points
            print("{:>5d} {:>10.5f} {:>7.2f}".format(day, expansion, radius))

        # print the number of days
        # print("Number of days =", day)


* Following is the outputs of the print statements, which looks better than the output in the previous section, 

    .. code-block:: text
    
        $ python expansion.py "Gold" 8 
          day  expansion  radius
            1    0.64000    8.64
            2    0.69120    9.33
            3    0.74650   10.08  


Saving results in file
======================

There are two ways to save the results in the file. 

* In the first method, we can redirect the output of the print statement to the file (instead of printing then on the terminal). Note that the '>' sign will overwrite the file contents, whereas the '>>' sign will append the content at the end of the file. Also, please see the `Unix Guide <http://unixguide.readthedocs.io/en/latest/index.html>`_ to learn more commands like 'cat' which is used to display the content of the file. 
  
    .. code-block:: text
        :emphasize-lines: 1, 9

        $ python expansion.py "Gold" 8 > data.txt 

        $ cat data.txt  
          day  expansion  radius
            1    0.64000    8.64
            2    0.69120    9.33
            3    0.74650   10.08

        $ python expansion.py "Gold" 8 >> data.txt
  
        $ cat data.txt  
          day  expansion  radius
            1    0.64000    8.64
            2    0.69120    9.33
            3    0.74650   10.08
          day  expansion  radius
            1    0.64000    8.64
            2    0.69120    9.33
            3    0.74650   10.08

* In the other method, we can open the file in the Python script and then save the data in the file, as shown below. More file operations will be discussed in next chapter. 

    .. note:: 

        This method is better than previous method, as we can select the outputs which should be written in the file. For example, Line 44 will not be printed in the file, if the wrong input values are provided during execution of the script. 
    
    .. code-block:: python
        :linenos:
        :emphasize-lines: 21, 29-30, 54-55

        # expansion.py

        # find the number of days when radius = 10 cm due to heat-expansion

        import sys

        if len(sys.argv) != 3: # display error message for missing arguments
            raise SystemExit("usage : ring.py \"metal\" radius")

        # sys.argv[0] is the file name 
        metal = sys.argv[1] 

        # input is read as string therefore it is converted into float
        radius = float(sys.argv[2])

        # list of expansion rate for different metal
        rate = [0.4, 0.08, 0.05] # [Copper, Gold, Iron]

        day = 0

        out_file = open("expansion.txt", "w") # open file in write mode

        ## use any of the below
        ## below is not in good format
        # print("{0}, {1}, {2}".format("day", "expansion", "radius"))

        ## "> right aligned" "< left aligned"
        ## 5s : string with width 5
        print("{:>5s} {:>10s} {:>7s}".format("day", "expansion", "radius"), 
                        file = out_file)

        ## old style
        # print("%5s %10s %7s" % ("day", "expansion", "radius"))

        while radius < 10:
            # multiply by correct expansion rate
            if metal == "Copper":
                expansion = radius * rate[0]
            elif metal == "Gold":
                expansion = radius * rate[1] 
            elif metal == "Iron":
                expansion = radius * rate[2]
            else: 
                print("Enter the correct metal")
                break

            # new radius
            radius += expansion
            day += 1 # increment the number of days by one
            
            ## print the data
            ## 5d : 5 digits 
            ## 7.2f : 7 digits with 2 decimal points
            print("{:>5d} {:>10.5f} {:>7.2f}".format(day, expansion, radius), 
                        file = out_file)

        # print the number of days
        # print("Number of days =", day)


* Now, execute the script and see the content of the file as below, 
  
    .. code-block:: text
    
        $ python expansion.py "Gold" 8 
        
        $ cat expansion.txt  
          day  expansion  radius
            1    0.64000    8.64
            2    0.69120    9.33
            3    0.74650   10.08 


Alter the printing sequence
===========================

We can alter the printing location of the arguments as below, 

.. code-block:: python
    
    # {0}, {1}, {2} are the position i.e. 0th argument i.e. 'day'
    # will go in first position and so on.
    print("{0}, {1}, {2}".format("day", "expansion", "radius"))

    # same as above but with modified locations i.e. parameter 2 (i.e. day)
    # will be printed at first position i.e. {2}
    print("{2}, {0}, {1}".format("expansion", "radius", "day"))


    # same can be done with following formatting
    print("{:>5s} {:>10s} {:>7s}".format("day", "expansion", "radius"))

    # same as above, but location is defined 
    print("{2:>5s} {0:>10s} {1:>7s}".format("expansion", "radius", "day"))



Conclusion
==========

In this chapter, we saw various print statements. Also, we learn the methods by which can save the outputs in the file. In the next chapter, we will learn some more file operations. 

















