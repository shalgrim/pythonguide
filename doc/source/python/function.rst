Functions
*********

docstring
=========

The content between the \'\'\' is known as 'docstring', which are displayed by the 'help' function as shown below. Press 'q' to exit from help-screen. 

.. code-block:: python

    >>> def add2Num(x, y): 
    ...     ''' add two numbers : x, y ''' 
    ...     print(x+y) 
    ...  
    >>> add2Num(2, 3) 
    5
    >>> help(add2Num) 
    Help on function add2Num in module __main__:

    add2Num(x, y)
        add two numbers : x, y


types of docstring
==================

There are two standard format of creating the docstrings, i.e. Numpy style and Goole style, which are supported by Sphinx-documentation for generating the auto-documentation of the project. 


Convert previous code into function
===================================

Conversion
----------

Now, we will convert the code in :numref:`py_price_without_func` into function. Conversion process is quite simple, as shown in :numref:`py_price_conv_func`, where function with docstring is defined at Lines 5-6. Ther previous code is indented and finally a return statement is added in Line 20. Lines 22-24 calls the function and print the output. Lastly, Lines 28-29 are the standard boilerplate to set the function 'main' as the entry point. 

.. code-block:: python
    :linenos:
    :caption: price calculation using function
    :emphasize-lines: 5-6, 19-20, 22-24, 26-29
    :name: py_price_conv_func

    # price.py

    import csv

    def ring_cost(filename):
        ''' calculate the total cost '''

        total_price = 0 # for all items in the list

        with open(filename, 'r') as f: # open file in read mode
            rows = csv.reader(f)
            header = next(rows) # skip line 1 i.e. header
            for row in rows:
                row[3] = float(row[3]) # price
                row[4] = int(row[4]) # quantity
            
                total_price += row[3] * row[4]

        # print("Total price = %10.2f" % total_price)
        return total_price  # return total_price

    def main():
        total = ring_cost('price.csv')  # function call
        print("Total price = %10.2f" % total) # print value

    # standard boilerplate
    # main is the starting function
    if __name__ == '__main__':
        main()


.. code-block:: text

    $ python price.py 
    Total price =    1328.38


Debugging
---------

In :numref:`py_price_without_func`, we have to manually change the file name in the 'price.py' file; whereas in :numref:`py_price_conv_func`, we can pass the filename as the parameter in the :numref:`python_shell_debug`, as shown below, 

.. code-block:: text

    $ python -i price.py  
    Total price =    1328.38
    >>> 
    >>> ring_cost('price.csv') 
    1328.3799999999999
    >>> ring_cost('price2.csv')
    1328.3799999999999
    >>>  

.. note:: 

    In the above command, 'python -i price.py', the main() function is called. And after entering the Python shell, we can call the function directly i.e. ring_cost('price.csv'), and the corresponding 'return value', i.e. 1328.3799999999999, will be printed in the shell. 


glob module
===========

'glob' module can be used to select the files for further processing, as shown in this section. To understand it, first create some files as below, 

.. code-block:: shell

    $ touch data1.txt data2.txt data3.txt data_1.txt data_2.txt data_3.txt data1.csv data2.csv data3.csv


Next, open the Python shell and see the following function of 'glob' module, 

.. code-block:: python

    >>> glob.glob('*.csv') # find all csv files
    ['data3.csv', 'data2.csv', 'data1.csv']

    >>> glob.glob('data*.txt') # select all txt file which starts with 'data'
    ['data_3.txt', 'data_2.txt', 'data2.txt', 'data_1.txt', 'data3.txt', 'data1.txt']

    >>> # select txt files which have one character between 'data' and '.txt'
    >>> glob.glob('data?.txt') 
    ['data2.txt', 'data3.txt', 'data1.txt']

    >>> glob.glob('data[0-2]*.csv') # select csv file with numbers 0,1,2 after 'data'
    ['data2.csv', 'data1.csv']


.. note:: 

    * The 'glob' module returns the 'list'. 
    * The list is in unordered form. 


Price calculation on files using 'glob'
---------------------------------------

Now, we will use the glob module to perform 'price calculation' on files using 'glob' module. First open the Python shell without using '-i' operation. 


.. code-block:: shell

    $ python


Since we did not use the '-i' operation to open the shell, therefore we need to import the function 'ring_cost' in the shell, as shown below, 

.. code-block:: python
    :emphasize-lines: 2
    
    >>> import glob
    >>> from price import ring_cost
    >>> 
    >>> files = glob.glob('pri*.csv')
    >>> for file in files:
    ...     print(file, ring_cost(file))
    ... 
    price.csv 1328.3799999999999
    price2.csv 1328.3799999999999


.. warning::

    Note that we need to restart python shell every time, we made some changes in the code. This is required as the 'import' statement loads all the data at the first time; and when we re-import the modules then it is fetched from the cache. 


Note that, the 'glob' returns a list, therefore we can extend the list using various listing operation. This can be useful when we have files names with different names, but required same operations e.g. we want to perform price calculation on another set of items which has same columns as price.csv file. List can be modified as below, 


.. code-block:: python

    >>> files = glob.glob('pri*.csv')
    >>> files2 = glob.glob('pri*.csv')
    >>> files.extend(files2)  # extend list
    >>> files
    ['price.csv', 'price2.csv', 'price.csv', 'price2.csv']
    >>> files.append('price2.csv')  # append data
    >>> files
    ['price.csv', 'price2.csv', 'price.csv', 'price2.csv', 'price2.csv']

